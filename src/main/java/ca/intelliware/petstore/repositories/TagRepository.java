package ca.intelliware.petstore.repositories;

import ca.intelliware.petstore.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository
    extends JpaRepository<Tag, Long> {

    boolean existsByName(String name);

    Tag findByName(String name);
}
