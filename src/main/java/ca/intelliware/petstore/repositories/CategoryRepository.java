package ca.intelliware.petstore.repositories;

import ca.intelliware.petstore.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository
    extends JpaRepository<Category, Long> {

    boolean existsByName(String name);

    Category findByName(String name);
}
