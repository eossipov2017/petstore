package ca.intelliware.petstore.repositories;

import ca.intelliware.petstore.model.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository
    extends JpaRepository<Pet, Long> {
}
