package ca.intelliware.petstore.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import static ca.intelliware.petstore.model.UserRole.*;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.security.config.http.SessionCreationPolicy.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration
    extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;

    @Autowired
    public SecurityConfiguration(PasswordEncoder passwordEncoder, UserDetailsService userDetailsService) {
        this.passwordEncoder = passwordEncoder;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/user/**").fullyAuthenticated()

            .antMatchers(GET, "/pet/**").hasAnyAuthority(MANAGER.toString(), VIEWER.toString())
            .antMatchers(POST, "/pet/**").hasAuthority(MANAGER.toString())
            .antMatchers(PUT, "/pet/**").hasAuthority(MANAGER.toString())
            .antMatchers(DELETE, "/pet/**").hasAuthority(MANAGER.toString())

            .antMatchers(GET, "/category/**").hasAnyAuthority(MANAGER.toString(), VIEWER.toString())
            .antMatchers(POST, "/category/**").hasAuthority(MANAGER.toString())
            .antMatchers(PUT, "/category/**").hasAuthority(MANAGER.toString())
            .antMatchers(DELETE, "/category/**").hasAuthority(MANAGER.toString())

            .antMatchers(GET, "/tag/**").hasAnyAuthority(MANAGER.toString(), VIEWER.toString())
            .antMatchers(POST, "/tag/**").hasAuthority(MANAGER.toString())
            .antMatchers(PUT, "/tag/**").hasAuthority(MANAGER.toString())
            .antMatchers(DELETE, "/tag/**").hasAuthority(MANAGER.toString())

            .anyRequest().permitAll()
            .and()
            .httpBasic()
            .and()
            .sessionManagement().sessionCreationPolicy(STATELESS)
            .and()
            .logout()
            .logoutSuccessUrl("/")
            .and()
            .csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(OPTIONS, "/**");
    }

}
