package ca.intelliware.petstore.security;

import ca.intelliware.petstore.model.User;
import ca.intelliware.petstore.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Optional;

import static java.util.Optional.*;

@Service
public class UserDetailsProvider {

    private final UserRepository userRepository;

    @Autowired
    public UserDetailsProvider(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> getUser(Principal principal) {
        return getUserId(principal).map(userRepository::findOne);
    }

    public Optional<Long> getUserId(Principal principal) {
        return ofNullable(principal)
            .map(p -> (Authentication) principal)
            .filter(Authentication::isAuthenticated)
            .map(authentication -> (CustomUserDetails) authentication.getPrincipal())
            .map(CustomUserDetails::getUserId);
    }
}
