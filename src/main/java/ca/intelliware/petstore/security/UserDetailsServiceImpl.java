package ca.intelliware.petstore.security;

import ca.intelliware.petstore.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static ca.intelliware.petstore.model.UserStatus.*;
import static java.util.Collections.*;

@Service
public class UserDetailsServiceImpl
    implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName)
        throws UsernameNotFoundException {
        return userRepository.findByUsername(userName)
            .map(user -> {
                LOGGER.info("User found: " + user);
                return new CustomUserDetails(user.getId(), user.getUsername(), user.getPassword(),
                    user.getUserStatus() == UNLOCKED, true, true,
                    user.getUserStatus() == UNLOCKED,
                    singletonList(new SimpleGrantedAuthority(user.getUserRole().name())));
            })
            .orElseThrow(() -> new UsernameNotFoundException(userName));
    }
}
