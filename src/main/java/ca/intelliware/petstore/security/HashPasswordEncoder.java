package ca.intelliware.petstore.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Optional.*;
import static org.apache.commons.lang3.StringUtils.*;

@Service
public class HashPasswordEncoder
    implements PasswordEncoder {

    @Override
    public String encode(CharSequence rawPassword) {
        return ofNullable(rawPassword)
            .map(CharSequence::toString)
            .map(String::trim)
            .filter(value -> !isEmpty(value))
            .map(value -> {
                try {
                    final MessageDigest digest = MessageDigest.getInstance("SHA-256");
                    final byte[] hash = digest.digest(value.getBytes(StandardCharsets.UTF_8));
                    return IntStream.range(0, hash.length).mapToObj(
                        i -> Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1)).collect(
                        Collectors.joining());
                } catch (NoSuchAlgorithmException ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }

            })
            .orElse(null);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return Objects.equals(encode(rawPassword), encodedPassword);
    }
}
