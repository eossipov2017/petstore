package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.BaseEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static java.util.Optional.*;

class BaseController<T extends BaseEntity> {

    private final String path;

    BaseController(String path) {
        this.path = ofNullable(path)
            .filter(StringUtils::isNotBlank)
            .map(value -> value.startsWith("/") ? value : "/" + value)
            .map(value -> value.endsWith("/") ? value : value + "/")
            .orElseThrow(IllegalArgumentException::new);
    }

    ResponseEntity<ErrorInfo> errorResponse(HttpStatus status, Exception ex) {
        return ResponseEntity
            .status(status)
            .body(new ErrorInfo(status.getReasonPhrase(), ex.getClass().getName(), ex.getMessage(), status.value()));
    }

    URI location(UriComponentsBuilder builder, String segment) {
        return builder
            .path(path)
            .path(segment)
            .build().toUri();
    }
}
