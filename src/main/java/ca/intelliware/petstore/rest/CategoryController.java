package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.Category;
import ca.intelliware.petstore.repositories.CategoryRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;
import java.util.Objects;

import static java.util.Optional.*;
import static org.apache.commons.lang3.StringUtils.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/category")
public class CategoryController
    extends BaseController<Category> {

    private final CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        super("category");
        this.categoryRepository = categoryRepository;
    }

    @Transactional
    @RequestMapping(method = POST)
    public ResponseEntity<Category> add(@RequestBody Category category,
        UriComponentsBuilder builder) {
        if (category == null) {
            throw new IllegalArgumentException();
        }
        if (category.getId() != null && category.getId() != 0L) {
            return update(category, builder);
        }

        if (categoryRepository.existsByName(category.getName())) {
            throw new ConflictException("Category with name '" + category.getName() + "' already exists.");
        }

        final Category persisted = categoryRepository.save(category);
        return ResponseEntity
            .created(location(builder, persisted.getId().toString()))
            .body(persisted);
    }

    @ExceptionHandler({DataIntegrityViolationException.class, IllegalArgumentException.class})
    public ResponseEntity<ErrorInfo> badRequestException(Exception ex) {
        return errorResponse(BAD_REQUEST, ex);
    }

    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<ErrorInfo> conflictException(ConflictException ex) {
        return errorResponse(CONFLICT, ex);
    }

    @RequestMapping(method = GET, path = "/count")
    public long count() {
        return categoryRepository.count();
    }

    @Transactional
    @RequestMapping(method = DELETE, path = "/{id}")
    @ResponseStatus(OK)
    public void delete(@PathVariable long id) {
        categoryRepository.delete(id);
    }

    @RequestMapping(method = GET, path = "/{id}")
    public ResponseEntity<Category> find(@PathVariable long id) {
        return ofNullable(categoryRepository.findOne(id))
            .map(ResponseEntity::ok)
            .orElseThrow(() -> new NotFoundException("Category with id [" + id + "] is not found."));
    }

    @RequestMapping(method = GET)
    public Collection<Category> list(Pageable pageable) {
        return categoryRepository.findAll(pageable).getContent();
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> notFoundException(NotFoundException ex) {
        return errorResponse(NOT_FOUND, ex);
    }

    @RequestMapping(method = PUT)
    @Transactional
    public ResponseEntity<Category> update(@RequestBody Category category,
        UriComponentsBuilder builder) {
        if (category == null) {
            throw new IllegalArgumentException();
        }
        if (category.getId() == null || category.getId() == 0L) {
            return add(category, builder);
        }

        if (!categoryRepository.exists(category.getId())) {
            throw new NotFoundException("Category with id [" + category.getId() + "] is not found.");
        }

        final Category existing = categoryRepository.findByName(category.getName());
        if (existing != null && equalsIgnoreCase(category.getName(), existing.getName())) {
            if (Objects.equals(category.getId(), existing.getId())) {
                return ResponseEntity
                    .ok()
                    .location(location(builder, existing.getId().toString()))
                    .body(existing);
            } else {
                throw new ConflictException(
                    "Another category [" + existing.getId() + "] with name '" + category.getName() + "' already exists.");
            }
        }
        final Category persisted = categoryRepository.save(category);
        return ResponseEntity
            .ok()
            .location(location(builder, persisted.getId().toString()))
            .body(persisted);
    }
}
