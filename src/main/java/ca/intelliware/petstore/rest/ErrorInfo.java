package ca.intelliware.petstore.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ErrorInfo
    implements Serializable {

    private static final long serialVersionUID = -6670277133191503485L;

    private String error;
    private String exception;
    private String message;
    private int status;
}
