package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.User;
import ca.intelliware.petstore.security.UserDetailsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserDetailsProvider userDetailsProvider;

    @Autowired
    public UserController(UserDetailsProvider userDetailsProvider) {
        this.userDetailsProvider = userDetailsProvider;
    }

    @RequestMapping(method = GET, path = "/logged")
    public ResponseEntity<User> currentUser(@AuthenticationPrincipal Principal principal) {
        return userDetailsProvider.getUser(principal)
            .map(user -> {
                final User userWithoutPassword = user.copy();
                userWithoutPassword.setPassword(null);
                return userWithoutPassword;
            })
            .map(ResponseEntity::ok)
            .orElseThrow(() -> new SecurityException("User has not been authenticated"));
    }

    private ResponseEntity<ErrorInfo> errorResponse(HttpStatus status, Exception ex) {
        return ResponseEntity
            .status(status)
            .body(new ErrorInfo(status.getReasonPhrase(), ex.getClass().getName(), ex.getMessage(), status.value()));
    }

    @ExceptionHandler(SecurityException.class)
    public ResponseEntity<ErrorInfo> securityException(SecurityException ex) {
        return errorResponse(UNAUTHORIZED, ex);
    }
}
