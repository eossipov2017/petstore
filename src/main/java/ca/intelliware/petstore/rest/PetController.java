package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.Pet;
import ca.intelliware.petstore.repositories.PetRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;

import static java.util.Optional.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/pet")
public class PetController
    extends BaseController<Pet> {

    private final PetRepository petRepository;

    public PetController(PetRepository petRepository) {
        super("pet");
        this.petRepository = petRepository;
    }

    @Transactional
    @RequestMapping(method = POST)
    public ResponseEntity<Pet> add(@RequestBody Pet pet, UriComponentsBuilder builder) {
        if (pet == null) {
            throw new IllegalArgumentException();
        }

        if (pet.getId() != null && pet.getId() != 0L) {
            return update(pet, builder);
        }

        final Pet persisted = petRepository.save(pet);
        return ResponseEntity
            .created(location(builder, persisted.getId().toString()))
            .body(persisted);
    }

    @ExceptionHandler({DataIntegrityViolationException.class, IllegalArgumentException.class})
    public ResponseEntity<ErrorInfo> badRequestException(Exception ex) {
        return errorResponse(BAD_REQUEST, ex);
    }

    @RequestMapping(method = GET, path = "/count")
    public long count() {
        return petRepository.count();
    }

    @Transactional
    @RequestMapping(method = DELETE, path = "/{id}")
    @ResponseStatus(OK)
    public void delete(@PathVariable long id) {
        petRepository.delete(id);
    }

    @RequestMapping(method = GET, path = "/{id}")
    public ResponseEntity<Pet> find(@PathVariable long id) {
        return ofNullable(petRepository.findOne(id))
            .map(ResponseEntity::ok)
            .orElseThrow(() -> new NotFoundException("Pet with id [" + id + "] is not found."));
    }

    @RequestMapping(method = GET)
    public Collection<Pet> list(Pageable pageable) {
        return petRepository.findAll(pageable).getContent();
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> notFoundException(NotFoundException ex) {
        return errorResponse(NOT_FOUND, ex);
    }

    @Transactional
    @RequestMapping(method = PUT)
    public ResponseEntity<Pet> update(@RequestBody Pet pet, UriComponentsBuilder builder) {
        if (pet == null) {
            throw new IllegalArgumentException();
        }
        if (pet.getId() == null || pet.getId() == 0L) {
            return add(pet, builder);
        }

        if (!petRepository.exists(pet.getId())) {
            throw new NotFoundException("Pat with id [" + pet.getId() + "] is not found.");
        }

        final Pet persisted = petRepository.save(pet);
        return ResponseEntity
            .ok()
            .location(location(builder, persisted.getId().toString()))
            .body(persisted);
    }
}
