package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.Tag;
import ca.intelliware.petstore.repositories.TagRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;
import java.util.Objects;

import static java.util.Optional.*;
import static org.apache.commons.lang3.StringUtils.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/tag")
public class TagController
    extends BaseController<Tag> {

    private final TagRepository tagRepository;

    public TagController(TagRepository tagRepository) {
        super("tag");
        this.tagRepository = tagRepository;
    }

    @Transactional
    @RequestMapping(method = POST)
    public ResponseEntity<Tag> add(@RequestBody Tag tag,
        UriComponentsBuilder builder) {
        if (tag == null) {
            throw new IllegalArgumentException();
        }
        if (tag.getId() != null && tag.getId() != 0L) {
            return update(tag, builder);
        }
        if (tagRepository.existsByName(tag.getName())) {
            throw new ConflictException("Tag with name '" + tag.getName() + "' already exists.");
        }
        final Tag persisted = tagRepository.save(tag);
        return ResponseEntity
            .created(location(builder, persisted.getId().toString()))
            .body(persisted);
    }

    @ExceptionHandler({DataIntegrityViolationException.class, IllegalArgumentException.class})
    public ResponseEntity<ErrorInfo> badRequestException(Exception ex) {
        return errorResponse(BAD_REQUEST, ex);
    }

    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<ErrorInfo> conflictException(ConflictException ex) {
        return errorResponse(CONFLICT, ex);
    }

    @RequestMapping(method = GET, path = "/count")
    public long count() {
        return tagRepository.count();
    }

    @Transactional
    @RequestMapping(method = DELETE, path = "/{id}")
    @ResponseStatus(OK)
    public void delete(@PathVariable long id) {
        tagRepository.delete(id);
    }

    @RequestMapping(method = GET, path = "/{id}")
    public ResponseEntity<Tag> find(@PathVariable long id) {
        return ofNullable(tagRepository.findOne(id))
            .map(ResponseEntity::ok)
            .orElseThrow(() -> new NotFoundException("Tag with id [" + id + "] is not found."));
    }

    @RequestMapping(method = GET)
    public Collection<Tag> list(Pageable pageable) {
        return tagRepository.findAll(pageable).getContent();
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> notFoundException(NotFoundException ex) {
        return errorResponse(NOT_FOUND, ex);
    }

    @Transactional
    @RequestMapping(method = PUT)
    public ResponseEntity<Tag> update(@RequestBody Tag tag, UriComponentsBuilder builder) {
        if (tag == null) {
            throw new IllegalArgumentException();
        }
        if (tag.getId() == null || tag.getId() == 0L) {
            return add(tag, builder);
        }

        if (!tagRepository.exists(tag.getId())) {
            throw new NotFoundException("Tag with id [" + tag.getId() + "] is not found.");
        }

        final Tag existing = tagRepository.findByName(tag.getName());
        if (existing != null && equalsIgnoreCase(tag.getName(), existing.getName())) {
            if (Objects.equals(tag.getId(), existing.getId())) {
                return ResponseEntity
                    .ok()
                    .location(location(builder, existing.getId().toString()))
                    .body(existing);
            } else {
                throw new ConflictException(
                    "Another tag [" + existing.getId() + "] with name '" + tag.getName() + "' already exists.");
            }
        }
        final Tag persisted = tagRepository.save(tag);
        return ResponseEntity
            .ok()
            .location(location(builder, persisted.getId().toString()))
            .body(persisted);
    }
}
