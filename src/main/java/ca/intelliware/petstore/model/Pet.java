package ca.intelliware.petstore.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.*;

@Entity
@Table(indexes = {@Index(columnList = "name")})
public class Pet
    extends NamedEntity {

    private static final long serialVersionUID = -3469158214439111332L;
    @Getter
    @Setter
    @ManyToOne(fetch = EAGER, optional = false)
    private Category category;
    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    private PetStatus status;
    @ManyToMany(fetch = EAGER, cascade = {PERSIST})
    private List<Tag> tags;

    private Pet(Pet other) {
        super(other);
        this.category = other.category;
        this.status = other.status;
        this.tags = other.tags != null ? new ArrayList<>(other.tags) : null;
    }

    public Pet() {
    }

    public Pet(Long id) {
        super(id);
    }

    public Pet(Long id, String name) {
        super(id, name);
    }

    @Override
    public Pet copy() {
        return new Pet(this);
    }

    public List<Tag> getTags() {
        return tags == null ? (tags = new ArrayList<>()) : tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    protected String stringify() {
        return super.stringify()
            + ",category=" + category
            + ",status=" + status
            + ",tags=" + tags;
    }
}
