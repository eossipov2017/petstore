package ca.intelliware.petstore.model;

public enum UserStatus {

    UNLOCKED, LOCKED
}
