package ca.intelliware.petstore.model;

public enum UserRole {
    MANAGER, VIEWER
}
