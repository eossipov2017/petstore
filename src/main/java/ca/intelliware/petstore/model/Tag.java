package ca.intelliware.petstore.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Tag
    extends NamedEntity {

    private static final long serialVersionUID = -87183260727964158L;

    private Tag(Tag other) {
        super(other);
    }

    public Tag() {
    }

    public Tag(Long id) {
        super(id);
    }

    public Tag(Long id, String name) {
        super(id, name);
    }

    @Override
    public Tag copy() {
        return new Tag(this);
    }
}
