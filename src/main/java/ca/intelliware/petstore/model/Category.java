package ca.intelliware.petstore.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Category
    extends NamedEntity {

    private static final long serialVersionUID = -1541478250524153173L;

    private Category(Category other) {
        super(other);
    }

    public Category() {
    }

    public Category(Long id) {
        super(id);
    }

    public Category(Long id, String name) {
        super(id, name);
    }

    @Override
    public Category copy() {
        return new Category(this);
    }
}
