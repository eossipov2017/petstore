package ca.intelliware.petstore.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class NamedEntity
    extends BaseEntity {

    private static final long serialVersionUID = 6765190143851497322L;
    @Getter
    @Setter
    @Basic(optional = false)
    private String name;

    NamedEntity(NamedEntity other) {
        super(other);
        this.name = other.name;
    }

    public NamedEntity() {
    }

    public NamedEntity(Long id) {
        super(id);
    }

    public NamedEntity(Long id, String name) {
        super(id);
        this.name = name;
    }

    @Override
    String stringify() {
        return super.stringify() + ",name=" + name;
    }
}
