package ca.intelliware.petstore.model;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@MappedSuperclass
public abstract class BaseEntity
    implements Serializable {
    private static final long serialVersionUID = -3246546372821656817L;
    @Getter
    @Setter
    @Id
    @GeneratedValue
    private Long id;

    BaseEntity(BaseEntity other) {
        this.id = other.id;
    }

    public abstract BaseEntity copy();

    String stringify() {
        return "id=" + id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" + stringify() + '}';
    }
}
