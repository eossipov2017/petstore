package ca.intelliware.petstore.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table
public class User
    extends BaseEntity {

    private static final long serialVersionUID = -4165539584699941471L;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String firstName;
    @Getter
    @Setter
    private String lastName;
    @Getter
    @Setter
    @Basic(optional = false)
    private String password;
    @Getter
    @Setter
    private String phone;
    @Getter
    @Setter
    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    private UserRole userRole;
    @Getter
    @Setter
    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;
    @Getter
    @Setter
    @Basic(optional = false)
    @Column(unique = true)
    private String username;

    private User(User other) {
        super(other);
        this.email = other.email;
        this.firstName = other.firstName;
        this.lastName = other.lastName;
        this.password = other.password;
        this.phone = other.phone;
        this.userStatus = other.userStatus;
        this.username = other.username;
        this.userRole = other.userRole;
    }

    public User(Long id) {
        super(id);
    }

    public User() {
    }

    @Override
    public User copy() {
        return new User(this);
    }

    @Override
    protected String stringify() {
        return super.stringify()
            + ",email=" + email
            + ",firstName=" + firstName
            + ",lastName=" + lastName
            + ",phone=" + phone
            + ",userRole=" + userRole
            + ",userStatus=" + userStatus
            + ",username=" + username;
    }
}
