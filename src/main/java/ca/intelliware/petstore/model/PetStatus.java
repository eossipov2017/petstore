package ca.intelliware.petstore.model;

public enum PetStatus {
    AVAILABLE, PENDING, SOLD
}
