import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {AlertComponent} from './alert.component';
import {AlertService} from "../../services/alert.service";
import {By} from "@angular/platform-browser";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpErrorResponse} from "@angular/common/http";

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AlertService
      ],
      declarations: [AlertComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([AlertService], () => {
      expect(component).toBeTruthy();
    })
  );

  it('should display alert', inject([AlertService], (service) => {
      let rootElement = fixture.debugElement.query(By.css('div'));
      expect(rootElement.nativeElement.textContent.trim()).toBe('');

      let errorResponse = new HttpErrorResponse({error: {error: 'error', message: 'message'}});
      service.alert(errorResponse);

      fixture.detectChanges();

      let instance = fixture.debugElement.componentInstance;
      expect(instance.alert).toBe('error: message');

      let debugElement = rootElement
        .query(By.css('div'));
      let element = debugElement.nativeElement;
      expect(element.textContent).toContain('Error!');
      expect(element.textContent).toContain(instance.alert);
    })
  );
});
