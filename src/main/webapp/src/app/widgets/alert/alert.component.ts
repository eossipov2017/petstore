import {Component, OnInit} from '@angular/core';
import {AlertService} from "../../services/alert.service";

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html'
})
export class AlertComponent
  implements OnInit {
  private _alert: string = '';

  constructor(private alertService: AlertService) {
  }

  get alert(): string {
    return this._alert;
  }

  ngOnInit() {
    this.alertService.subscribe(alert => this._alert = alert);
  }

  remove() {
    this.alertService.clear();
  }
}
