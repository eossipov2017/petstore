import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SortComponent} from './sort.component';
import {By} from "@angular/platform-browser";

describe('SortComponent', () => {
  let component: SortComponent;
  let fixture: ComponentFixture<SortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SortComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sets the column title', () => {
    component.title = 'Sample';
    fixture.detectChanges();
    let element = fixture.debugElement.query(By.css('a'));
    expect(element.nativeElement.textContent).toContain(component.title);
  });

  it('cycles the sort order', () => {
    let actual: string = '';

    component.column = 'name';
    component.registerOnChange(value => actual = value);

    let link = fixture.debugElement.query(By.css('a'));
    let icon = link.query(By.css('span'));

    let values: string[] = ['', 'name,asc', 'name,desc'];
    let icons: string[] = ['glyphicon-sort', 'glyphicon-sort-by-alphabet', 'glyphicon-sort-by-alphabet-alt'];

    for (let i = 1; i <= 4; i++) {

      link.triggerEventHandler('click', null);

      fixture.detectChanges();

      expect(component.value).toBe(values[i % values.length]);
      expect(actual).toBe(component.value);
      expect(icon.classes[icons[i % icons.length]]).toBe(true);
    }
  });

  it('sets the value (default)', () => {
    let actual: string;
    component.registerOnChange(value => actual = value);

    let icon = fixture.debugElement.query(By.css('a span'));

    component.writeValue(10);

    fixture.detectChanges();

    expect(component.value).toBe('');
    expect(actual).toBeUndefined();
    expect(icon.classes['glyphicon-sort']).toBe(true);
  });

  it('sets the value (name,asc)', () => {
    let actual: string;
    component.registerOnChange(value => actual = value);
    component.column = 'name';

    let icon = fixture.debugElement.query(By.css('a span'));

    component.writeValue('name,asc');

    fixture.detectChanges();

    expect(component.value).toBe('name,asc');
    expect(actual).toBeUndefined();
    expect(icon.classes['glyphicon-sort-by-alphabet']).toBe(true);
  });

  it('sets the value (name,desc)', () => {
    let actual: string;
    component.registerOnChange(value => actual = value);
    component.column = 'name';

    let icon = fixture.debugElement.query(By.css('a span'));

    component.writeValue('name, desc');

    fixture.detectChanges();

    expect(component.value).toBe('name,desc');
    expect(actual).toBeUndefined();
    expect(icon.classes['glyphicon-sort-by-alphabet-alt']).toBe(true);
  });
});
