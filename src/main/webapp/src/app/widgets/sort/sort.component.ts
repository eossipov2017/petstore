import {Component, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

enum OrderEnum {
  NONE = 0,
  ASC,
  DESC
}

const orderValues: string[] = ['', 'asc', 'desc'];
const pattern = /(\w+)\s*,\s*(asc|desc)/i;

@Component({
  selector: 'app-sort',
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: SortComponent, multi: true}
  ],
  templateUrl: './sort.component.html'
})
export class SortComponent
  implements OnInit, ControlValueAccessor {
  private _column: string;
  private _disabled: boolean = false;
  private _onChange = (_: string) => {
  };
  private _onTouched = () => {
  };
  private _order: OrderEnum = OrderEnum.ASC;
  private _sortBy: string;
  private _title: string;
  private _value: string;

  constructor() {
  }

  get column(): string {
    return this._column;
  }

  @Input()
  set column(value: string) {
    this._column = value;
  }

  get disabled(): boolean {
    return this._disabled;
  }

  @Input()
  set disabled(value: boolean) {
    this._disabled = value;
  }

  get title(): string {
    return this._title;
  }

  @Input()
  set title(value: string) {
    this._title = value;
  }

  get value(): string {
    return this._value;
  }

  asc(): boolean {
    return this._sortBy == this.column && this._order == OrderEnum.ASC;
  }

  desc(): boolean {
    return this._sortBy == this.column && this._order == OrderEnum.DESC;
  }

  ngOnInit() {
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

  sort(): void {
    if (this.column != this._sortBy) {
      this._sortBy = this.column;
      this._order = OrderEnum.NONE;
    }
    this._order = (this._order + 1) % 3;
    this.updateValue();
    this._onTouched();
    this._onChange(this._value);
  }

  unsorted(): boolean {
    return this._sortBy != this.column || this._order == OrderEnum.NONE;
  }

  private updateValue() {
    this._value = this._order == OrderEnum.NONE ? '' : this._sortBy + ',' + orderValues[this._order];
  }

  writeValue(obj: any): void {
    const exec = pattern.exec(obj);
    if (exec) {
      this._sortBy = exec[1];
      this._order = orderValues.findIndex((value2, index, obj) => value2 == exec[2]);
    } else {
      this._sortBy = '';
      this._order = OrderEnum.NONE;
    }
    this.updateValue();
  }
}
