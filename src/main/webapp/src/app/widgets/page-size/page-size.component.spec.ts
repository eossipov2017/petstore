import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PageSizeComponent} from './page-size.component';
import {By} from "@angular/platform-browser";

describe('PageSizeComponent', () => {
  let component: PageSizeComponent;
  let fixture: ComponentFixture<PageSizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PageSizeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSizeComponent);
    component = fixture.componentInstance;

    component.sizes = [10, 30, 50];
    component.value = 30;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('checks the initial state', () => {
    let button = fixture.debugElement.query(By.css('button'));
    expect(button.nativeElement.textContent).toContain(`${component.value}`);

    let items = fixture.debugElement.queryAll(By.css('ul li'));
    expect(items.length).toBe(component.sizes.length);
    for (let i = 0; i < items.length; i++) {
      let textContent = items[i].query(By.css('a')).nativeElement.textContent;
      expect(textContent).toBe(`${component.sizes[i]}`);
    }
  });

  it('changes the size', () => {
    let actual: number = 0;

    component.registerOnChange(value => actual = value);

    let firstElement = fixture.debugElement.query(By.css('ul li a'));
    firstElement.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(component.value).toBe(10);
    expect(actual).toBe(10);
    expect(`${component.value}`).toBe(firstElement.nativeElement.textContent);
  });
});
