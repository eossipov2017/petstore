import {Component, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-page-size',
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: PageSizeComponent, multi: true}
  ],
  templateUrl: './page-size.component.html'
})
export class PageSizeComponent
  implements OnInit, ControlValueAccessor {
  private _disabled: boolean = false;
  private _onChange = (_: number) => {
  };
  private _onTouched = () => {
  };
  private _sizes: number[];
  private _value: number;

  constructor() {
  }

  get disabled(): boolean {
    return this._disabled;
  }

  @Input()
  set disabled(value: boolean) {
    this._disabled = value;
  }

  get sizes(): number[] {
    return this._sizes;
  }

  @Input()
  set sizes(value: number[]) {
    this._sizes = value;
    this.value = value[0];
  }

  get value(): number {
    return this._value;
  }

  set value(value: number) {
    if (this._value != value) {
      this._value = value;
      this._onChange(value);
    }
  }

  ngOnInit() {
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this._value = obj;
  }
}
