import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {PetsComponent} from "../components/pets/pets.component";
import {CategoriesComponent} from "../components/categories/categories.component";
import {TagsComponent} from "../components/tags/tags.component";
import {PetComponent} from "../components/pet/pet.component";
import {LoginComponent} from "../components/login/login.component";
import {AuthGuardService} from "../guard/auth-guard.service";

const routes: Routes = [
  {path: '', redirectTo: '/pets', pathMatch: 'full'},
  {path: 'pets', component: PetsComponent, canActivate: [AuthGuardService]},
  {path: 'pets/:id', component: PetComponent, canActivate: [AuthGuardService]},
  {path: 'categories', component: CategoriesComponent, canActivate: [AuthGuardService]},
  {path: 'tags', component: TagsComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
}
