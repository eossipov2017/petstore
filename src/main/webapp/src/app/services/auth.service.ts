import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {User} from "../model/user";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Authentication} from "./authentication";

@Injectable()
export class AuthService
  implements Authentication {

  private _currentUser: User;
  private _headers: HttpHeaders = new HttpHeaders({'Authorization': ''});

  constructor(private http: HttpClient) {
  }

  get currentUser(): User {
    return this._currentUser;
  }

  get headers(): HttpHeaders {
    return this._headers;
  }

  login(username: string, password): Observable<User> {
    let headers = new HttpHeaders({
      'Authorization': 'Basic ' + btoa(`${username}:${password}`),
    });
    this.logout();
    return this.http.get("/user/logged", {headers: headers})
      .map(response => {
        this._currentUser = response as User;
        this._headers = headers;
        return this._currentUser;
      });
  }

  logout(): void {
    this._currentUser = null;
    this._headers = new HttpHeaders({'Authorization': ''});
  }
}
