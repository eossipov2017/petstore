import {Injectable} from '@angular/core';
import {RepositoryService} from "./repository.service";
import {Tag} from "../model/tag";
import {AuthService} from "./auth.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class TagService
  extends RepositoryService<Tag> {

  constructor(protected http: HttpClient, authService: AuthService) {
    super(http, authService, "tag");
  }
}
