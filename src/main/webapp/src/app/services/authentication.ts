import {User} from "../model/user";
import {HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

export interface Authentication {

  currentUser: User;

  headers: HttpHeaders;

  login(username: string, password): Observable<User>;

  logout(): void;
}
