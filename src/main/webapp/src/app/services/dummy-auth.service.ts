import {Authentication} from "./authentication";
import {User} from "../model/user";
import {HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

export class DummyAuthService
  implements Authentication {

  private _httpHeaders = new HttpHeaders();
  private _user = new User(11, 'john.doe');

  constructor() {
  }

  get currentUser(): User {
    return this._user;
  }

  get headers(): HttpHeaders {
    return this._httpHeaders;
  }

  login(username: string, password): Observable<User> {
    this._httpHeaders = new HttpHeaders({
      'Authorization': 'Basic ' + btoa(`${username}:${password}`),
    });
    this._user = new User(17, username);
    return Observable.of(this._user);
  }

  logout(): void {
    this._httpHeaders = new HttpHeaders();
    this._user = null;
  }
}
