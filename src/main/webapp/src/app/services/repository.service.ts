import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";
import {Pageable} from "../model/pageable";
import {BaseEntity} from "../model/base-entity";
import {Repository} from "./repository";
import {AuthService} from "./auth.service";
import {HttpClient, HttpParams} from "@angular/common/http";

export class RepositoryService<T extends BaseEntity>
  implements Repository<T> {

  constructor(protected http: HttpClient, protected authService: AuthService, protected resource: string) {
  }

  add(value: T): Observable<T> {
    let headers = this.authService.headers
      .append('Content-Type', 'application/json');
    return this.http.post<T>(this.url(), value, {headers: headers});
  }

  count(filterBy?: string): Observable<number> {
    let params = new HttpParams();
    if (filterBy) {
      params = params.append('filter', filterBy);
    }
    return this.http.get<number>(this.url("count"), {headers: this.authService.headers, params: params});
  }

  find(id: number): Observable<T> {
    return this.http.get<T>(this.url(id), {headers: this.authService.headers});
  }

  list(pageable: Pageable): Observable<T[]> {
    let params = new HttpParams()
      .append('page', pageable.page.toString())
      .append('size', pageable.size.toString());
    if (pageable.sort) {
      params = pageable.sort.reduce((params, value) => params.append('sort', value), params);
    }
    if (pageable.filterBy) {
      params = params.append('filter', pageable.filterBy);
    }

    return this.http.get<T[]>(this.url(), {headers: this.authService.headers, params: params});
  }

  remove(id: number): Observable<any> {
    return this.http.delete(this.url(id), {headers: this.authService.headers});
  }

  update(value: T): Observable<T> {
    let headers = this.authService.headers
      .append('Content-Type', 'application/json');
    return this.http.put<T>(this.url(), value, {headers: headers});
  }

  protected url(...resources): string {
    return ['', this.resource].concat(resources).join("/");
  }
}
