import {inject, TestBed} from '@angular/core/testing';

import {AlertService} from './alert.service';
import {NavigationStart, Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpErrorResponse} from "@angular/common/http";

describe('AlertService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AlertService
      ]
    });
  });

  it('should be created', inject([AlertService], (service: AlertService) => {
    expect(service).toBeTruthy();
  }));

  it('alert is expected to contain error and message', inject([AlertService], (service) => {
      let message;
      service.subscribe(msg => message = msg);

      let errorResponse = new HttpErrorResponse({error: {error: 'error', message: 'message'}});
      service.alert(errorResponse);
      expect(message).toBe('error: message');
    }
  ));

  it('alert expected to contain error', inject([AlertService], (service) => {
      let message;
      service.subscribe(msg => message = msg);

      let errorResponse = new HttpErrorResponse({error: {error: 'error'}});
      service.alert(errorResponse);
      expect(message).toBe('error');
    }
  ));

  it('alert is expected to contain message', inject([AlertService], (service) => {
      let message;
      service.subscribe(msg => message = msg);

      let errorResponse = new HttpErrorResponse({error: {message: 'message'}});
      service.alert(errorResponse);
      expect(message).toBe('message');
    }
  ));

  it('message expected to be cleared on navigation', inject([AlertService, Router], (service, router) => {
      let message;
      service.subscribe(msg => message = msg);

      let errorResponse = new HttpErrorResponse({error: {error: 'error', message: 'message'}});
      service.alert(errorResponse);

      router.events.next(new NavigationStart(0, ''));
      expect(message).toBeUndefined();
    }
  ));

  it('message expected to be cleared', inject([AlertService], (service) => {
      let message;
      service.subscribe(msg => message = msg);

      let errorResponse = new HttpErrorResponse({error: {error: 'error', message: 'message'}});
      service.alert(errorResponse);

      service.clear();
      expect(message).toBeUndefined();
    }
  ));
});
