import {BaseEntity} from "../model/base-entity";
import {Observable} from "rxjs/Observable";
import {Pageable} from "../model/pageable";

export interface Repository<T extends BaseEntity> {

  add(value: T): Observable<T>;

  count(filterBy?: string): Observable<number>;

  find(id: number): Observable<T>;

  list(pageable: Pageable): Observable<T[]>;

  remove(id: number): Observable<any>;

  update(value: T): Observable<T>;
}
