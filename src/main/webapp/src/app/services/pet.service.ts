import {Injectable} from '@angular/core';
import {RepositoryService} from "./repository.service";
import {Pet} from "../model/pet";
import {AuthService} from "./auth.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PetService
  extends RepositoryService<Pet> {

  constructor(protected http: HttpClient, authService: AuthService) {
    super(http, authService, "pet");
  }
}
