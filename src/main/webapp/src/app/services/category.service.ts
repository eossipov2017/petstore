import {Injectable} from '@angular/core';
import {RepositoryService} from "./repository.service";
import {Category} from "../model/category";
import {AuthService} from "./auth.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CategoryService
  extends RepositoryService<Category> {

  constructor(protected http: HttpClient, authService: AuthService) {
    super(http, authService, "category");
  }
}
