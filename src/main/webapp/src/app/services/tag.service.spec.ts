import {fakeAsync, inject, TestBed} from '@angular/core/testing';

import {TagService} from './tag.service';
import {Tag} from "../model/tag";
import {Pageable} from "../model/pageable";
import {AuthService} from "./auth.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {DummyAuthService} from "./dummy-auth.service";

describe('TagService', () => {

  let entity = 'tag';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: AuthService, useClass: DummyAuthService},
        TagService,
      ]
    });
  });

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('should be created', inject([TagService], (service: TagService) => {
    expect(service).toBeTruthy();
  }));

  it('adds tag', inject([TagService, HttpTestingController],
    fakeAsync((service, httpMock) => {

      let id = 1;
      let name = `${entity}_${id}`;
      let expected: Tag = new Tag(id, name);

      service.add(expected).subscribe(actual => {
        expect(actual.id).toEqual(id);
        expect(actual.name).toEqual(expected.name);
      });
      // At this point, the request is pending, and no response has been
      // sent. The next step is to expect that the request happened.
      const req = httpMock.expectOne(`/${entity}`);

      // If no request with that URL was made, or if multiple requests match,
      // expectOne() would throw. However this test makes only one request to
      // this URL, so it will match and return a mock request. The mock request
      // can be used to deliver a response or make assertions against the
      // request. In this case, the test asserts that the request is a GET.
      expect(req.request.method).toEqual('POST');

      // Next, fulfill the request by transmitting a response.
      req.flush(req.request.body);
    })
  ));

  it('retrieves count using filter', inject([TagService, HttpTestingController],
    fakeAsync((service, httpMock) => {
      let filter = 'test';
      let expected: number = 10;

      service.count(filter).subscribe(actual => expect(actual).toEqual(expected));

      const req = httpMock.expectOne(`/${entity}/count?filter=${filter}`);
      expect(req.request.method).toEqual('GET');
      req.flush(expected);
    })
  ));

  it('retrieves tag by id', inject([TagService, HttpTestingController],
    fakeAsync((service, httpMock) => {

      let id = 1;
      let name = `${entity}_${id}`;

      service.find(1).subscribe(actual => {
        expect(actual.id).toEqual(id);
        expect(actual.name).toEqual(name);
      });

      const req = httpMock.expectOne(`/${entity}/${id}`);
      expect(req.request.method).toEqual('GET');
      req.flush({"id": id, "name": name});
    })
  ));

  it('retrieves tags', inject([TagService, HttpTestingController],
    fakeAsync((service, httpMock) => {

      let pageable: Pageable = {
        page: 1,
        size: 35,
        sort: [
          'id,asc'
        ],
        filterBy: 'test'
      };

      service.list(pageable).subscribe(actual => {
        expect(actual.length).toEqual(pageable.size);
        for (let i = 0; i < pageable.size; i++) {
          let category = actual[i];
          let id = pageable.page * pageable.size + i;
          expect(category.id).toEqual(id);
          expect(category.name).toEqual(`${entity}_${id}`);
        }
      });

      const req = httpMock.expectOne(`/${entity}?page=${pageable.page}&size=${pageable.size}&sort=${pageable.sort[0]}&filter=${pageable.filterBy}`);
      expect(req.request.method).toEqual('GET');

      let items: Tag[] = [];
      for (let i = 0; i < pageable.size; i++) {
        let id = pageable.page * pageable.size + i;
        items.push(new Tag(id, `${entity}_${id}`));
      }
      req.flush(items);
    })
  ));

  it('removes tag by id', inject([TagService, HttpTestingController],
    fakeAsync((service, httpMock) => {

      let id = 1;

      service.remove(id).subscribe(actual => expect(actual).not.toBeUndefined());

      const req = httpMock.expectOne(`/${entity}/${id}`);
      expect(req.request.method).toEqual('DELETE');
      req.flush({});
    })
  ));

  it('updates tag', inject([TagService, HttpTestingController],
    fakeAsync((service, httpMock) => {

      let id = 1;
      let name = `${entity}_${id}`;
      let expected: Tag = new Tag(id, name);

      service.update(expected).subscribe(actual => {
        expect(actual.id).toEqual(expected.id);
        expect(actual.name).toEqual(expected.name);
      });

      const req = httpMock.expectOne(`/${entity}`);
      expect(req.request.method).toEqual('PUT');
      req.flush(req.request.body);
    })
  ));
});
