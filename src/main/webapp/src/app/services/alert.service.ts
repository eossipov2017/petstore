import {Injectable} from '@angular/core';
import {NavigationStart, Router} from "@angular/router";
import {Subject} from "rxjs/Subject";
import 'rxjs/add/operator/distinctUntilChanged';

@Injectable()
export class AlertService {
  private _subject = new Subject<string>();

  constructor(private router: Router) {
    // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
    router.events
      .distinctUntilChanged((x, y) => (x instanceof NavigationStart) && (y instanceof NavigationStart))
      .subscribe(event => {
        if (event instanceof NavigationStart) {
          this.clear();
        }
      });
  }

  get subject(): Subject<string> {
    return this._subject;
  }

  alert(response: any) {
    let json = response.error;
    let message = [json.error, json.message].filter(value => value && value != '').join(": ");
    if (!message || message == '') {
      message = JSON.stringify(json);
    }
    this._subject.next(message);
  }

  clear() {
    this._subject.next();
  }

  subscribe(observer: (value: string) => void) {
    this._subject
      .distinctUntilChanged()
      .subscribe(observer);
  }
}
