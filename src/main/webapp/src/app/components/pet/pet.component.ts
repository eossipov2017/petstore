import {Component, OnInit} from '@angular/core';
import {Location} from "@angular/common";
import {ActivatedRoute, Params} from "@angular/router";
import {PetService} from "../../services/pet.service";
import {Pet, PET_STATUSES, PetStatus} from "../../model/pet";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/of";
import {CategoryService} from "../../services/category.service";
import {TagService} from "../../services/tag.service";
import {Category} from "../../model/category";
import {Tag} from "../../model/tag";
import {AlertService} from "../../services/alert.service";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  providers: [
    CategoryService,
    PetService,
    TagService
  ]
})
export class PetComponent
  implements OnInit {
  private _availableCategories: Category[] = [];
  private _availableTags: Tag[] = [];
  private _pet: Pet = {id: 0, name: '', status: 'AVAILABLE', tags: []};

  constructor(private route: ActivatedRoute,
              private location: Location,
              private petService: PetService,
              private categoryService: CategoryService,
              private tagService: TagService,
              private alertService: AlertService,
              private authService: AuthService) {
  }

  get availableCategories(): Category[] {
    return this._availableCategories;
  }

  get availableTags(): Tag[] {
    return this._availableTags.filter(tag => !this._pet.tags.find(other => tag.id == other.id));
  }

  get pet(): Pet {
    return this._pet;
  }

  get statuses(): PetStatus[] {
    return PET_STATUSES;
  }

  compare(pet1: Pet, pet2: Pet): boolean {
    return (pet1 && pet2)
      ? pet1.id == pet2.id
      : pet1 === pet2;
  }

  goBack(): void {
    this.location.back();
  }

  ngOnInit() {
    this.categoryService.list({page: 0, size: 100, sort: ['name,asc']})
      .subscribe(categories => this._availableCategories = categories,
        error => this.alertService.alert(error));
    this.tagService.list({page: 0, size: 100, sort: ['name,asc']})
      .subscribe(tags => this._availableTags = tags,
        error => this.alertService.alert(error));

    this.route.params
      .switchMap((params: Params) => {
        let id = +params['id'];
        let newPet: Pet = {id: 0, name: '', status: 'AVAILABLE', tags: []};

        return id != 0 ? this.petService.find(id) : Observable.of(newPet);
      })
      .subscribe(pet => this._pet = pet,
        error => this.alertService.alert(error));
  }

  notAllowed(): boolean {
    return !(this.authService.currentUser && this.authService.currentUser.userRole == 'MANAGER');
  }

  removeTag(tag: Tag): void {
    this._pet.tags =
      this._pet.tags.filter(value => !(tag && value ? tag.id == value.id : tag === value));
  }

  save(): void {
    ((!this._pet.id || this._pet.id == 0)
      ? this.petService.add(this._pet)
      : this.petService.update(this._pet))
      .subscribe(pet => this.goBack(),
        error => this.alertService.alert(error));
  }
}
