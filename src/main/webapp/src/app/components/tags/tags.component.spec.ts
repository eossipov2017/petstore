import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TagsComponent} from './tags.component';
import {SortComponent} from "../../widgets/sort/sort.component";
import {PaginationComponent, PaginationConfig} from "ngx-bootstrap";
import {PageSizeComponent} from "../../widgets/page-size/page-size.component";
import {RouterTestingModule} from "@angular/router/testing";
import {FormsModule} from "@angular/forms";
import {AlertService} from "../../services/alert.service";
import {TagService} from "../../services/tag.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {DummyAuthService} from "../../services/dummy-auth.service";
import {AuthService} from "../../services/auth.service";

describe('TagsComponent', () => {
  let component: TagsComponent;
  let fixture: ComponentFixture<TagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TagsComponent, PageSizeComponent, PaginationComponent, SortComponent],
      imports: [
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        TagService,
        AlertService,
        PaginationConfig,
        {provide: AuthService, useClass: DummyAuthService},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
