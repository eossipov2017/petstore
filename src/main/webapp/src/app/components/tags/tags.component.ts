import {Component} from '@angular/core';
import {Tag} from "../../model/tag";
import {ListComponent} from "../base/list-component";
import {TagService} from "../../services/tag.service";
import {AlertService} from "../../services/alert.service";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  providers: [
    TagService
  ]
})
export class TagsComponent
  extends ListComponent<Tag> {

  private _nameValue: string;

  constructor(private tagService: TagService, alertService: AlertService, authService: AuthService) {
    super(alertService, authService);
    this.countAction = filter => this.tagService.count(filter);
    this.listAction = pageable => this.tagService.list(pageable);
    this.removeAction = id => this.tagService.remove(id);
  }

  get nameValue(): string {
    return this._nameValue;
  }

  set nameValue(value: string) {
    this._nameValue = value;
  }

  save(): void {
    this.tagService.add(new Tag(0, this._nameValue))
      .subscribe(category => this.load(),
        error => this.alertService.alert(error));
  }
}
