import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent
  implements OnInit {

  constructor(private authService: AuthService, private router: Router) {
  }

  get logged(): boolean {
    return !!this.authService.currentUser;
  }

  get userName(): string {
    let user = this.authService.currentUser;
    if (!user) {
      return '(unauthenticated)';
    }
    let name = [user.firstName, user.lastName]
      .filter(value => value)
      .map(value => value.trim())
      .filter(value => value != '')
      .join(' ');
    if (!name || name == '') {
      name = user.email;
    }
    if (!name || name == '') {
      name = user.username;
    }
    return name;
  }

  logout(): void {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }

  ngOnInit() {
  }
}
