import {Component} from '@angular/core';
import {PetService} from "../../services/pet.service";
import {Pet} from "../../model/pet";
import {ListComponent} from "../base/list-component";
import {AlertService} from "../../services/alert.service";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  providers: [
    PetService
  ]
})
export class PetsComponent
  extends ListComponent<Pet> {

  constructor(private petService: PetService, alertService: AlertService, authService: AuthService) {
    super(alertService, authService);
    this.countAction = filter => this.petService.count(filter);
    this.listAction = pageable => this.petService.list(pageable);
    this.removeAction = id => this.petService.remove(id);
  }
}
