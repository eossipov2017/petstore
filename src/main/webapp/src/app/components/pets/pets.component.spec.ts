import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PetsComponent} from './pets.component';
import {RouterTestingModule} from "@angular/router/testing";
import {FormsModule} from "@angular/forms";
import {SortComponent} from "../../widgets/sort/sort.component";
import {PaginationComponent, PaginationConfig} from "ngx-bootstrap";
import {PageSizeComponent} from "../../widgets/page-size/page-size.component";
import {NamedListPipe} from "../../pipes/named-list.pipe";
import {AlertService} from "../../services/alert.service";
import {PetService} from "../../services/pet.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {DummyAuthService} from "../../services/dummy-auth.service";
import {AuthService} from "../../services/auth.service";

describe('PetsComponent', () => {
  let component: PetsComponent;
  let fixture: ComponentFixture<PetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PetsComponent, PageSizeComponent, PaginationComponent, SortComponent, NamedListPipe],
      imports: [
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        PetService,
        AlertService,
        PaginationConfig,
        {provide: AuthService, useClass: DummyAuthService},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
