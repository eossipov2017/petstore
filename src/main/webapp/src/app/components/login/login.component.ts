import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {AlertService} from "../../services/alert.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent
  implements OnInit {
  private _password: string;
  private _returnUrl: string;
  private _userName: string;

  constructor(private authService: AuthService,
              private alertService: AlertService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get userName(): string {
    return this._userName;
  }

  set userName(value: string) {
    this._userName = value;
  }

  login(): void {
    // login successful so redirect to return url
    this.authService.login(this.userName, this.password)
      .subscribe(user => this.router.navigateByUrl(this._returnUrl),
        error => this.alertService.alert(error));
  }

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this._returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
}
