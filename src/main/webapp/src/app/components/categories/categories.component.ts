import {Component} from '@angular/core';
import {ListComponent} from "../base/list-component";
import {Category} from "../../model/category";
import {CategoryService} from "../../services/category.service";
import {AlertService} from "../../services/alert.service";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  providers: [
    CategoryService
  ]
})
export class CategoriesComponent
  extends ListComponent<Category> {
  private _nameValue: string;

  constructor(private categoryService: CategoryService, alertService: AlertService, authService: AuthService) {
    super(alertService, authService);
    this.countAction = filter => this.categoryService.count(filter);
    this.listAction = pageable => this.categoryService.list(pageable);
    this.removeAction = id => this.categoryService.remove(id);
  }

  get nameValue(): string {
    return this._nameValue;
  }

  set nameValue(value: string) {
    this._nameValue = value;
  }

  save(): void {
    this.categoryService.add(new Category(0, this._nameValue))
      .subscribe(category => this.load(),
        error => this.alertService.alert(error));
  }
}
