import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CategoriesComponent} from './categories.component';
import {FormsModule} from "@angular/forms";
import {PageSizeComponent} from "../../widgets/page-size/page-size.component";
import {PaginationComponent, PaginationConfig} from "ngx-bootstrap";
import {SortComponent} from "../../widgets/sort/sort.component";
import {CategoryService} from "../../services/category.service";
import {AlertService} from "../../services/alert.service";
import {RouterTestingModule} from "@angular/router/testing";
import {Category} from "../../model/category";
import {Repository} from "../../services/repository";
import {Observable} from "rxjs/Observable";
import {Pageable} from "../../model/pageable";
import "rxjs/add/observable/empty";
import {By} from "@angular/platform-browser";
import {DummyAuthService} from "../../services/dummy-auth.service";
import {AuthService} from "../../services/auth.service";

export class CategoryServiceStub
  implements Repository<Category> {

  private _items: Category[] = [];

  constructor() {
    for (let i = 0; i < 100; i++) {
      this._items.push(new Category(i, `Category_${i}`));
    }
  }

  add(value: Category): Observable<Category> {
    if (!value) {
      return Observable.empty();
    }

    if (value.id && value.id != 0) {
      return this.update(value);
    }

    let id = this._items.map(c => c.id).reduce((r, i) => (r < i) ? i : r, 0) + 1;
    let category = new Category(id, value.name);
    this._items.push(category);
    return Observable.of(category);
  }

  count(filterBy?: string): Observable<number> {
    return Observable.of(this._items.length);
  }

  find(id: number): Observable<Category> {
    let category = this._items.find(item => item.id == id);
    return category ? Observable.of(category) : Observable.empty();
  }

  list(pageable: Pageable): Observable<Category[]> {
    let start = pageable.page * pageable.size;
    let page = this._items;
    let sort = pageable.sort && pageable.sort.length > 0 ? pageable.sort[0] : '';
    if (sort != '') {
      let params = sort.split(',');
      switch (params[1]) {
        case 'asc':
          page = page.sort((a, b) => {
            return a[params[0]] == b[params[0]]
              ? 0
              : (a[params[0]] > b[params[0]] ? 1 : -1);
          });
          break;
        case 'desc':
          page = page.sort((a, b) => {
            return a[params[0]] == b[params[0]]
              ? 0
              : (b[params[0]] > a[params[0]] ? 1 : -1);
          });
          break;
        default:
          break;
      }
    }
    return Observable.of(page.slice(start, start + pageable.size));
  }

  remove(id: number): Observable<any> {
    this._items = this._items.filter(item => item.id != id);
    return Observable.of(id);
  }

  update(value: Category): Observable<Category> {
    let index = this._items.findIndex(item => item.id == value.id);
    if (index >= 0 && index < this._items.length) {
      this._items[index] = value;
    }
    return Observable.of(value);
  }
}

describe('CategoriesComponent', () => {
  let component: CategoriesComponent;
  let fixture: ComponentFixture<CategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CategoriesComponent, PageSizeComponent, PaginationComponent, SortComponent],
      imports: [FormsModule, RouterTestingModule],
      providers: [
        {provide: AuthService, useClass: DummyAuthService},
      ]
    })
    // This is required for providers defined at a component level
      .overrideComponent(CategoriesComponent, {
        set: {
          providers: [
            AlertService,
            PaginationConfig,
            {provide: CategoryService, useClass: CategoryServiceStub}
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('checks initial state', () => {
    let rows = fixture.debugElement.queryAll(By.css('table tbody tr'));

    expect(component.currentPage).toBe(1);
    expect(rows.length).toBe(component.itemsPerPage);

    let cells = rows[0].queryAll(By.css('td'));
    expect(cells[0].nativeElement.textContent).toBe('[000]');
  });

  it('switches pages', () => {
    component.sortBy = 'id,asc';
    let pages = fixture.debugElement.queryAll(By.css('pagination a'));
    let page = pages[pages.length - 3]; // Last visible page
    page.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.currentPage).toBe(Number.parseInt(page.nativeElement.textContent));

    let cells = fixture.debugElement.queryAll(By.css('table tbody tr td'));
    expect(cells[0].nativeElement.textContent).toBe('[075]');
  });

  it('changes sort order', () => {
    component.sortBy = 'id,asc';

    let sort = fixture.debugElement
      .query(By.css('table thead app-sort'));

    sort.query(By.css('a')).triggerEventHandler('click', null);

    fixture.detectChanges();

    let icon = sort.query(By.css('a span'));
    expect(icon.classes['glyphicon-sort-by-alphabet']).toBe(true);

    sort.query(By.css('a')).triggerEventHandler('click', null);

    fixture.detectChanges();

    let cells = fixture.debugElement.queryAll(By.css('table tbody tr td'));
    expect(cells[0].nativeElement.textContent).toBe('[099]');
  });

  it('removes item', () => {
    let categoryService = fixture.debugElement.injector.get(CategoryService);
    let authService = fixture.debugElement.injector.get(AuthService);
    let spy = spyOn(categoryService, 'remove').and.callThrough();

    authService.currentUser.userRole = 'MANAGER';

    component.sortBy = 'id,asc';

    fixture.detectChanges();

    let rows = fixture.debugElement.queryAll(By.css('table tbody tr'));
    let cells = rows[24].queryAll(By.css('td'));
    expect(cells[0].nativeElement.textContent).toBe('[024]');

    let button = cells[cells.length - 1].query(By.css('a'));
    button.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(spy.calls.count()).toBe(1);
    expect(spy.calls.argsFor(0)[0]).toBe(24);

    rows = fixture.debugElement.queryAll(By.css('table tbody tr'));
    cells = rows[24].queryAll(By.css('td'));
    expect(cells[0].nativeElement.textContent).toBe('[025]');
  });
});
