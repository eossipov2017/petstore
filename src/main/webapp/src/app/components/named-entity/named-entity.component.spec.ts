import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NamedEntityComponent} from './named-entity.component';
import {FormsModule} from "@angular/forms";

describe('NamedEntityComponent', () => {
  let component: NamedEntityComponent;
  let fixture: ComponentFixture<NamedEntityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NamedEntityComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamedEntityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
