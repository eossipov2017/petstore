import {OnInit} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {BaseEntity} from "../../model/base-entity";
import {Pageable} from "../../model/pageable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {AlertService} from "../../services/alert.service";
import {AuthService} from "../../services/auth.service";

export abstract class ListComponent<T extends BaseEntity>
  implements OnInit {
  private _currentPage: number = 1;
  private _filterBy: string = "";
  private _items: T[];
  private _itemsPerPage: number = 25;
  private _sortBy: string = "name,asc";
  private _totalItems: number = 0;
  protected countAction: (filter: string) => Observable<number>;
  protected listAction: (pageable: Pageable) => Observable<T[]>;
  protected removeAction: (id: number) => Observable<number>;

  constructor(protected alertService: AlertService, protected authService: AuthService) {
  }

  get currentPage(): number {
    return this._currentPage;
  }

  set currentPage(value: number) {
    if (this._currentPage != value) {
      this._currentPage = value;
      this.load();
    }
  }

  get filterBy(): string {
    return this._filterBy;
  }

  set filterBy(value: string) {
    if (this._filterBy != value) {
      this._filterBy = value;
      this.load();
    }
  }

  get items(): T[] {
    return this._items;
  }

  get itemsPerPage(): number {
    return this._itemsPerPage;
  }

  set itemsPerPage(value: number) {
    if (this._itemsPerPage != value) {
      this._itemsPerPage = value;
      this.load();
    }
  }

  get sortBy(): string {
    return this._sortBy;
  }

  set sortBy(value: string) {
    if (this._sortBy != value) {
      this._sortBy = value;
      this.load();
    }
  }

  get totalItems(): number {
    return this._totalItems;
  }

  isNotAllowed(): boolean {
    return !(this.authService.currentUser && this.authService.currentUser.userRole == 'MANAGER');
  }

  load(): void {
    this.countAction(this.filterBy)
      .map(count => this.setCount(count))
      .switchMap(pageable => this.listAction(pageable))
      .subscribe(items => this._items = items,
        error => this.alertService.alert(error));
  }

  ngOnInit(): void {
    this.load();
  }

  remove(item: T): void {
    this.removeAction(item.id)
      .subscribe(id => this.load(),
        error => this.alertService.alert(error))
  }

  protected setCount(count) {
    this._totalItems = count;
    let totalPages = Math.floor(this._totalItems / this._itemsPerPage);
    if ((this._totalItems % this._itemsPerPage) != 0) totalPages++;
    if (totalPages < this._currentPage) this._currentPage = totalPages;

    let sort = (this._sortBy) ? [this._sortBy] : [];

    return {
      page: this._currentPage - 1,
      size: this._itemsPerPage,
      sort: sort,
      filterBy: this._filterBy
    };
  }
}
