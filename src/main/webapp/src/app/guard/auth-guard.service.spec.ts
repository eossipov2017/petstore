import {inject, TestBed} from '@angular/core/testing';

import {AuthGuardService} from './auth-guard.service';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpHeaders} from "@angular/common/http";
import {User} from "../model/user";
import {AuthService} from "../services/auth.service";

class DummyAuthService {

  private _httpHeaders = new HttpHeaders();
  private _user = new User(11, 'john.doe');

  get currentUser(): User {
    return this._user;
  }

  get headers(): HttpHeaders {
    return this._httpHeaders;
  }
}

describe('AuthGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        {provide: AuthService, useClass: DummyAuthService},
        AuthGuardService
      ]
    });
  });

  it('should be created', inject([AuthGuardService], (service: AuthGuardService) => {
    expect(service).toBeTruthy();
  }));
});
