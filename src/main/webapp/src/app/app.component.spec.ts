import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {MenuComponent} from "./components/menu/menu.component";
import {AlertComponent} from "./widgets/alert/alert.component";
import {RouterTestingModule} from "@angular/router/testing";
import {AlertService} from "./services/alert.service";
import {DummyAuthService} from "./services/dummy-auth.service";
import {AuthService} from "./services/auth.service";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        AlertComponent,
        MenuComponent
      ],
      providers: [
        AlertService,
        {provide: AuthService, useClass: DummyAuthService},
      ],
      imports: [
        RouterTestingModule
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
