import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {PaginationModule} from "ngx-bootstrap/pagination";

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing/app-routing.module";
import {MenuComponent} from './components/menu/menu.component';
import {PetsComponent} from './components/pets/pets.component';
import {CategoriesComponent} from './components/categories/categories.component';
import {TagsComponent} from './components/tags/tags.component';
import {NamedEntityComponent} from './components/named-entity/named-entity.component';
import {PageSizeComponent} from './widgets/page-size/page-size.component';
import {SortComponent} from './widgets/sort/sort.component';
import {NamedListPipe} from './pipes/named-list.pipe';
import {PetComponent} from './components/pet/pet.component';
import {AlertComponent} from './widgets/alert/alert.component';
import {AlertService} from "./services/alert.service";
import {LoginComponent} from './components/login/login.component';
import {AuthService} from "./services/auth.service";
import {AuthGuardService} from "./guard/auth-guard.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    PetsComponent,
    CategoriesComponent,
    TagsComponent,
    NamedEntityComponent,
    PageSizeComponent,
    SortComponent,
    NamedListPipe,
    PetComponent,
    AlertComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    PaginationModule.forRoot()
  ],
  providers: [
    AlertService,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
