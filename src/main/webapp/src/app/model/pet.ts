import {NamedEntity} from "./named-entity";
import {Category} from "./category";
import {Tag} from "./tag";

export type PetStatus = 'AVAILABLE' | 'PENDING' | 'SOLD';

export const PET_STATUSES: PetStatus[] = ['AVAILABLE', 'PENDING', 'SOLD'];

export class Pet
  extends NamedEntity {

  constructor(public id: number = 0,
              public name: string = '',
              public category?: Category,
              public status: PetStatus = 'AVAILABLE',
              public tags: Tag[] = []) {
    super(id, name);
  }
}
