import {BaseEntity} from "./base-entity";

export class NamedEntity
  extends BaseEntity {

  constructor(public id: number, public name: string) {
    super(id);
  }
}
