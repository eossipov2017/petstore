export class Pageable {

  constructor(public page: number = 0,
              public size: number = 25,
              public sort: string[] = [],
              public filterBy?: string) {
  }
}
