import {BaseEntity} from "./base-entity";

export type UserRole = 'MANAGER' | 'VIEWER';

export type UserStatus = 'UNLOCKED' | 'LOCKED';

export class User
  extends BaseEntity {

  constructor(id: number,
              public username: string,
              public userRole: UserRole = 'VIEWER',
              public userStatus: UserStatus = 'UNLOCKED',
              public firstName?: string,
              public lastName?: string,
              public email?: string,
              public phone?: string) {
    super(id);
  }
}
