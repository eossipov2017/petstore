import {Pipe, PipeTransform} from '@angular/core';
import {NamedEntity} from "../model/named-entity";

@Pipe({
  name: 'namedList'
})
export class NamedListPipe
  implements PipeTransform {

  transform(value: NamedEntity[], args?: any): any {
    return value
      ? value
        .filter(item => item)
        .map(item => {
          if (item.name) {
            let name = item.name.trim();
            if (name != '') return name;
          }
          return item.id;
        })
        .sort().join(",")
      : '';
  }
}
