import {NamedListPipe} from './named-list.pipe';

describe('NamedListPipe', () => {
  it('create an instance', () => {
    const pipe = new NamedListPipe();
    expect(pipe).toBeTruthy();
  });

  it('empty array', () => {
    const pipe = new NamedListPipe();
    expect(pipe.transform([])).toBe('');
  });

  it('single-element array', () => {
    const pipe = new NamedListPipe();
    expect(pipe.transform([{id: 0, name: 'one'}])).toBe('one');
  });

  it('multi-element array', () => {
    const pipe = new NamedListPipe();
    expect(pipe.transform([{id: 0, name: 'one'}, {id: 1, name: 'two'}])).toBe('one,two');
  });

  it('null-element array', () => {
    const pipe = new NamedListPipe();
    expect(pipe.transform([null, {id: 1, name: 'two'}])).toBe('two');
  });

  it('empty-name array', () => {
    const pipe = new NamedListPipe();
    expect(pipe.transform([{id: 0, name: '  '}, {id: 1, name: 'two'}])).toBe('0,two');
  });
});
