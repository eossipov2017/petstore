package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.Category;
import ca.intelliware.petstore.repositories.CategoryRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import static ca.intelliware.petstore.TestUtils.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@WebMvcTest(CategoryController.class)
@Transactional
@AutoConfigureDataJpa
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
@EnableSpringDataWebSupport
public class CategoryControllerTest
    extends BaseControllerTest {

    @Autowired
    protected MockMvc mvc;
    private final Supplier<String> nameSupplier = () -> "Category_" + UUID.randomUUID().toString();
    @Autowired
    private CategoryRepository repository;

    public CategoryControllerTest() {
        super("/category");
    }

    @Override
    MockMvc getMvc() {
        return mvc;
    }

    private long nonExistingId() {
        return nonExistingId(repository.findAll());
    }

    @Before
    public void setUp() throws Exception {
        repository.save(Arrays.asList(
            new Category(0L, "Dog"),
            new Category(0L, "Cat"),
            new Category(0L, "Fish")
        ));
    }

    @Test
    public void testAdd() throws Exception {
        final Category category = new Category(0L, nameSupplier.get());

        testSave(post(baseUrl), category, status().isCreated(), repository::findOne);
    }

    @Test
    public void testAdd_BAD_REQUEST_1() throws Exception {
        testBadPostRequest("{}");
    }

    @Test
    public void testAdd_BAD_REQUEST_2() throws Exception {
        testBadPostRequest("{\"id\":0}");
    }

    @Test
    public void testAdd_BAD_REQUEST_3() throws Exception {
        testBadPostRequest("{\"id\":\"0\"}");
    }

    @Test
    public void testAdd_CONFLICT() throws Exception {
        final Category category = new Category(0L, "Cat");
        mvc.perform(request(post(baseUrl), category))
            .andExpect(status().isConflict());
    }

    @Test
    public void testAdd_Update() throws Exception {
        final Category category = repository.findAll().stream().findAny()
            .orElseThrow(RuntimeException::new).copy();
        category.setName(nameSupplier.get());

        testSave(post(baseUrl), category, status().isOk(), repository::findOne);
    }

    @Test
    public void testCount() throws Exception {
        testCount(repository.count());
    }

    @Test
    public void testDelete() throws Exception {
        final Category category = repository.findAll().stream().findAny().orElseThrow(RuntimeException::new);

        testDelete(category.getId(), id -> !repository.exists(id));
    }

    @Test
    public void testList() throws Exception {
        final MockHttpServletRequestBuilder requestBuilder = get(baseUrl);
        final List<Category> categories = repository.findAll();
        final ResultActions actions = mvc.perform(requestBuilder)
            .andExpect(status().isOk());
        final String content = actions
            .andReturn().getResponse().getContentAsString();

        final Type type = new TypeToken<List<Category>>() {
        }.getType();
        final List<Category> returned = new Gson().fromJson(content, type);
        assertEquals(categories.size(), returned.size());
        for (int i = 0; i < categories.size(); i++) {
            compareValues(categories.get(i), returned.get(i));
        }
    }

    @Test
    public void testUpdate() throws Exception {
        final Category category = repository.findAll().stream().findAny().orElseThrow(RuntimeException::new).copy();
        category.setName(nameSupplier.get());

        testSave(put(baseUrl), category, status().isOk(), repository::findOne);
    }

    @Test
    public void testUpdate_Add() throws Exception {
        final Category category = new Category(0L, nameSupplier.get());

        testSave(put(baseUrl), category, status().isCreated(), repository::findOne);
    }

    @Test
    public void testUpdate_CONFLICT() throws Exception {
        final Category category = repository.findByName("Dog").copy();
        category.setName("Cat");

        mvc.perform(request(put(baseUrl), category))
            .andExpect(status().isConflict());
    }

    @Test
    public void testUpdate_NOT_FOUND() throws Exception {
        final Category category = new Category(nonExistingId(), nameSupplier.get());

        mvc.perform(request(put(baseUrl), category))
            .andExpect(status().isNotFound());
    }
}