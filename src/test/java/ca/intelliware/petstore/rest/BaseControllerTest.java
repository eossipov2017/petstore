package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.BaseEntity;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static ca.intelliware.petstore.TestUtils.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

abstract class BaseControllerTest {

    final String baseUrl;

    BaseControllerTest(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    private void checkLocation(ResultActions actions, BaseEntity entity) throws Exception {
        actions.andExpect(header().string("location", containsString(baseUrl + "/" + entity.getId())));
    }

    abstract MockMvc getMvc();

    <T extends BaseEntity> long nonExistingId(List<T> list) {
        return list.stream()
            .mapToLong(BaseEntity::getId)
            .max()
            .orElse(0L) + 1;
    }

    MockHttpServletRequestBuilder request(MockHttpServletRequestBuilder builder,
        BaseEntity entity) {
        return request(builder, stringify(entity));
    }

    private MockHttpServletRequestBuilder request(MockHttpServletRequestBuilder builder,
        String content) {
        return builder
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(content);
    }

    private String stringify(BaseEntity entity) {
        return new Gson().toJson(entity);
    }

    void testBadPostRequest(String request) throws Exception {
        getMvc().perform(request(post(baseUrl), request))
            .andExpect(status().isBadRequest());
    }

    void testCount(Long expected) throws Exception {
        getMvc().perform(get(baseUrl + "/count"))
            .andExpect(status().isOk())
            .andExpect(content().string(Long.toString(expected)));
    }

    void testDelete(Long id, Predicate<Long> predicate) throws Exception {
        getMvc().perform(
            delete(baseUrl + "/{id}", id))
            .andExpect(status().isOk());
        assertTrue(predicate.test(id));
    }

    void testSave(MockHttpServletRequestBuilder builder, BaseEntity entity,
        ResultMatcher resultMatcher, Function<Long, BaseEntity> findFunction) throws Exception {
        final ResultActions actions = getMvc().perform(request(builder, entity))
            .andExpect(resultMatcher);
        final String content = actions
            .andReturn().getResponse().getContentAsString();
        final BaseEntity returned = new Gson().fromJson(content, entity.getClass());

        checkLocation(actions, returned);
        final BaseEntity persisted = findFunction.apply(returned.getId());
        assertNotNull(persisted);
        compareValues(entity, persisted, "id");
        compareValues(persisted, returned);
    }
}
