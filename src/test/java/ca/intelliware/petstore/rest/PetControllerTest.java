package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.*;
import ca.intelliware.petstore.repositories.PetRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import static ca.intelliware.petstore.TestUtils.*;
import static java.lang.Math.*;
import static java.util.Collections.*;
import static java.util.stream.Collectors.*;
import static java.util.stream.LongStream.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@WebMvcTest(PetController.class)
@EnableSpringDataWebSupport
public class PetControllerTest
    extends BaseControllerTest {

    private List<Category> categories;
    @Autowired
    private MockMvc mvc;
    private final Supplier<String> nameSupplier = () -> "Pet_" + UUID.randomUUID().toString();
    private final PetStatus[] petStatuses = PetStatus.values();
    @MockBean
    private PetRepository repository;
    private List<Tag> tags;

    public PetControllerTest() {
        super("/pet");
    }

    private Pet generateInstance(Supplier<String> nameSupplier) {
        final Pet pet = new Pet(0L);
        pet.setCategory(categories.get(randomIndex(categories.size())));
        pet.setName(nameSupplier.get());
        pet.setStatus(petStatuses[randomIndex(petStatuses.length)]);
        pet.setTags(singletonList(tags.get(randomIndex(tags.size()))));
        return pet;
    }

    private Pet generateInstance() {
        return generateInstance(nameSupplier);
    }

    @Override
    MockMvc getMvc() {
        return mvc;
    }

    private int randomIndex(int size) {
        return toIntExact(round(random() * (size - 1)));
    }

    @Before
    public void setUp() throws Exception {
        categories = range(0, 5)
            .mapToObj(i -> new Category(i, "Category_" + i))
            .collect(toList());

        tags = range(0, 5)
            .mapToObj(i -> new Tag(i, "Tag_" + i))
            .collect(toList());
    }

    @Test
    public void testAdd() throws Exception {
        final long id = 17;

        when(repository.save(any(Pet.class))).then(invocation -> {
            final Pet pet = invocation.<Pet>getArgument(0).copy();
            pet.setId(id);
            return pet;
        });

        final Pet pet = generateInstance();

        getMvc().perform(request(post(baseUrl), pet))
            .andExpect(status().isCreated())
            .andExpect(header().string("location", containsString(baseUrl + "/" + id)));

        verify(repository, times(1)).save(any(Pet.class));
        verifyNoMoreInteractions(repository);
    }

    /**
     * Testing IllegalArgumentException
     */
    @Test
    public void testAdd_BAD_REQUEST_1() throws Exception {
        when(repository.save(any(Pet.class))).thenThrow(IllegalArgumentException.class);
        testBadPostRequest("{}");
    }

    /**
     * Testing DataIntegrityViolationException
     */
    @Test
    public void testAdd_BAD_REQUEST_2() throws Exception {
        when(repository.save(any(Pet.class))).thenThrow(DataIntegrityViolationException.class);
        testBadPostRequest("{\"id\":0}");
    }

    @Test
    public void testAdd_Update() throws Exception {
        final long id = 17;

        when(repository.exists(id)).thenReturn(true);
        when(repository.save(any(Pet.class))).then(invocation -> invocation.<Pet>getArgument(0).copy());

        final Pet pet = generateInstance();
        pet.setId(id);

        getMvc().perform(request(post(baseUrl), pet))
            .andExpect(status().isOk())
            .andExpect(header().string("location", containsString(baseUrl + "/" + id)));

        final InOrder inOrder = inOrder(repository);
        inOrder.verify(repository, times(1)).exists(id);
        inOrder.verify(repository, times(1)).save(any(Pet.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testCount() throws Exception {
        final long expected = 47L;

        when(repository.count()).thenReturn(expected);

        getMvc().perform(get(baseUrl + "/count"))
            .andExpect(status().isOk())
            .andExpect(content().string(Long.toString(expected)));

        verify(repository, times(1)).count();
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testDelete() throws Exception {
        getMvc().perform(
            delete(baseUrl + "/{id}", 23L))
            .andExpect(status().isOk());
        verify(repository, times(1)).delete(23L);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testFind() throws Exception {
        final Pet entity = generateInstance();
        entity.setId(11L);

        when(repository.findOne(entity.getId())).thenReturn(entity);

        final MockHttpServletRequestBuilder requestBuilder = get(baseUrl + "/{id}", entity.getId())
            .contentType(MediaType.APPLICATION_JSON_UTF8);
        final String content = mvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();
        final BaseEntity returned = new Gson().fromJson(content, entity.getClass());
        compareValues(entity, returned);

        verify(repository, times(1)).findOne(entity.getId());
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testFind_NOT_FOUND() throws Exception {
        final long id = 11L;

        when(repository.findOne(anyLong())).thenReturn(null);

        final MockHttpServletRequestBuilder requestBuilder = get(baseUrl + "/{id}", id);
        mvc.perform(requestBuilder)
            .andExpect(status().isNotFound());

        verify(repository, times(1)).findOne(id);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testList() throws Exception {
        final List<Pet> pets = range(0, 10)
            .mapToObj(i -> {
                final Pet instance = generateInstance(() -> "Pet_" + i);
                instance.setId(i);
                return instance;
            })
            .collect(toList());
        when(repository.findAll(any(Pageable.class)))
            .then(invocation -> new PageImpl<>(pets, invocation.getArgument(0), pets.size()));

        final MockHttpServletRequestBuilder requestBuilder = get(baseUrl);

        final String content = mvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

        final Type type = new TypeToken<List<Pet>>() {
        }.getType();
        final List<Pet> returned = new Gson().fromJson(content, type);
        assertEquals(pets.size(), returned.size());
        for (int i = 0; i < pets.size(); i++) {
            compareValues(pets.get(i), returned.get(i));
        }
        verify(repository, times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testUpdate() throws Exception {
        final long id = 17;

        when(repository.exists(id)).thenReturn(true);
        when(repository.save(any(Pet.class))).then(invocation -> invocation.<Pet>getArgument(0).copy());

        final Pet pet = generateInstance();
        pet.setId(id);

        getMvc().perform(request(put(baseUrl), pet))
            .andExpect(status().isOk())
            .andExpect(header().string("location", containsString(baseUrl + "/" + id)));

        final InOrder inOrder = inOrder(repository);
        inOrder.verify(repository, times(1)).exists(id);
        inOrder.verify(repository, times(1)).save(any(Pet.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testUpdate_Add() throws Exception {
        final long id = 17;

        when(repository.save(any(Pet.class))).then(invocation -> {
            final Pet pet = invocation.<Pet>getArgument(0).copy();
            pet.setId(id);
            return pet;
        });

        final Pet pet = generateInstance();

        getMvc().perform(request(put(baseUrl), pet))
            .andExpect(status().isCreated())
            .andExpect(header().string("location", containsString(baseUrl + "/" + id)));

        verify(repository, never()).exists(anyLong());
        verify(repository, times(1)).save(any(Pet.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void testUpdate_NOT_FOUND() throws Exception {
        final Pet pet = generateInstance();
        pet.setId(11L);

        mvc.perform(request(put(baseUrl), pet))
            .andExpect(status().isNotFound());

        verify(repository, times(1)).exists(anyLong());
        verify(repository, never()).save(any(Pet.class));
        verifyNoMoreInteractions(repository);
    }
}