package ca.intelliware.petstore.rest;

import ca.intelliware.petstore.model.Tag;
import ca.intelliware.petstore.repositories.TagRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import static ca.intelliware.petstore.TestUtils.*;
import static java.util.stream.IntStream.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@WebMvcTest(TagController.class)
@Transactional
@AutoConfigureDataJpa
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
@EnableSpringDataWebSupport
public class TagControllerTest
    extends BaseControllerTest {

    @Autowired
    private MockMvc mvc;
    private final Supplier<String> nameSupplier = () -> "Tag_" + UUID.randomUUID().toString();
    @Autowired
    private TagRepository repository;

    public TagControllerTest() {
        super("/tag");
    }

    @Override
    MockMvc getMvc() {
        return mvc;
    }

    private long nonExistingId() {
        return nonExistingId(repository.findAll());
    }

    @Before
    public void setUp() throws Exception {
        range(0, 5)
            .mapToObj(i -> new Tag(0L, "Tag_" + i))
            .forEach(repository::save);
    }

    @Test
    public void testAdd() throws Exception {
        final Tag tag = new Tag(0L, nameSupplier.get());

        testSave(post(baseUrl), tag, status().isCreated(), repository::findOne);
    }

    @Test
    public void testAdd_BAD_REQUEST_1() throws Exception {
        testBadPostRequest("{}");
    }

    @Test
    public void testAdd_BAD_REQUEST_2() throws Exception {
        testBadPostRequest("{\"id\":0}");
    }

    @Test
    public void testAdd_BAD_REQUEST_3() throws Exception {
        testBadPostRequest("{\"id\":\"0\"}");
    }

    @Test
    public void testAdd_CONFLICT() throws Exception {
        final Tag tag = new Tag(0L, "Tag_0");

        mvc.perform(request(post(baseUrl), tag))
            .andExpect(status().isConflict());
    }

    @Test
    public void testAdd_Update() throws Exception {
        final Tag tag = repository.findAll().stream().findAny()
            .orElseThrow(RuntimeException::new).copy();
        tag.setName(nameSupplier.get());

        testSave(post(baseUrl), tag, status().isOk(), repository::findOne);
    }

    @Test
    public void testCount() throws Exception {
        testCount(repository.count());
    }

    @Test
    public void testDelete() throws Exception {
        final Tag tag = repository.findAll().stream().findAny().orElseThrow(RuntimeException::new);

        testDelete(tag.getId(), id -> !repository.exists(id));
    }

    @Test
    public void testList() throws Exception {
        final MockHttpServletRequestBuilder requestBuilder = get(baseUrl);
        final List<Tag> tags = repository.findAll();
        final ResultActions actions = mvc.perform(requestBuilder)
            .andExpect(status().isOk());
        final String content = actions
            .andReturn().getResponse().getContentAsString();

        final Type type = new TypeToken<List<Tag>>() {
        }.getType();
        final List<Tag> returned = new Gson().fromJson(content, type);
        assertEquals(tags.size(), returned.size());
        for (int i = 0; i < tags.size(); i++) {
            compareValues(tags.get(i), returned.get(i));
        }
    }

    @Test
    public void testUpdate() throws Exception {
        final Tag tag = repository.findAll().stream().findAny().orElseThrow(RuntimeException::new).copy();
        tag.setName(nameSupplier.get());

        testSave(put(baseUrl), tag, status().isOk(), repository::findOne);
    }

    @Test
    public void testUpdate_Add() throws Exception {
        final Tag tag = new Tag(0L, nameSupplier.get());

        testSave(put(baseUrl), tag, status().isCreated(), repository::findOne);
    }

    @Test
    public void testUpdate_CONFLICT() throws Exception {
        final Tag tag = repository.findByName("Tag_0").copy();
        tag.setName("Tag_1");

        mvc.perform(request(put(baseUrl), tag))
            .andExpect(status().isConflict());
    }

    @Test
    public void testUpdate_NOT_FOUND() throws Exception {
        final Tag tag = new Tag(nonExistingId(), nameSupplier.get());

        mvc.perform(request(put(baseUrl), tag))
            .andExpect(status().isNotFound());
    }
}