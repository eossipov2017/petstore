package ca.intelliware.petstore.model;

import ca.intelliware.petstore.TestUtils;
import org.junit.Test;

import static org.junit.Assert.*;

public class CategoryTest {

    @Test
    public void testCopy() throws Exception {

        final Category expected = new Category(11L, "Category");
        final Category actual = expected.copy();
        assertFalse(expected == actual);
        TestUtils.compareValues(expected, actual);
    }
}