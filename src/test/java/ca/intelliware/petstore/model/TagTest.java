package ca.intelliware.petstore.model;

import ca.intelliware.petstore.TestUtils;
import org.junit.Test;

import static org.junit.Assert.*;

public class TagTest {
    @Test
    public void testCopy() throws Exception {

        final Tag expected = new Tag(17L, "Tag");
        final Tag actual = expected.copy();
        assertFalse(expected == actual);
        TestUtils.compareValues(expected, actual);
    }
}