package ca.intelliware.petstore.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class NamedEntityTest {

    @Test
    public void getName() throws Exception {
        final long expectedId = 17L;
        final String expectedName = "OAK";
        final NamedEntity instance = instance(expectedId, expectedName);
        assertEquals((Long) expectedId, instance.getId());
        assertEquals(expectedName, instance.getName());
    }

    private NamedEntity instance(long id) {
        return new NamedEntity(id) {
            private static final long serialVersionUID = 4699026138313101287L;

            @Override
            public BaseEntity copy() {
                throw new UnsupportedOperationException("The method is not implemented yet.");
            }
        };
    }

    private NamedEntity instance(long id, String name) {
        return new NamedEntity(id, name) {
            private static final long serialVersionUID = 4699026138313101287L;

            @Override
            public BaseEntity copy() {
                throw new UnsupportedOperationException("The method is not implemented yet.");
            }
        };
    }

    @Test
    public void setName() throws Exception {
        final String expected = "PALM";
        final NamedEntity instance = instance(23L);
        instance.setName(expected);
        assertEquals(expected, instance.getName());
    }
}