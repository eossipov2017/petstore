package ca.intelliware.petstore.model;

import ca.intelliware.petstore.TestUtils;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import static java.util.Collections.*;
import static org.junit.Assert.*;

public class PetTest {

    @Test
    public void testCopy() throws Exception {

        final Pet expected = new Pet(13L, "Pet");
        expected.setCategory(new Category(31L, "Category"));
        expected.setStatus(PetStatus.SOLD);
        expected.setTags(singletonList(new Tag(43L, "Tag")));
        final Pet actual = expected.copy();
        assertFalse(expected == actual);
        TestUtils.compareValues(expected, actual);
        actual.getTags().clear();
        assertEquals(expected.getTags().size() - 1, actual.getTags().size());
    }

    @Test
    public void testGetTags_1() throws Exception {

        final Pet pet = new Pet(17L, "Some Pet");
        final Field field = pet.getClass().getDeclaredField("tags");
        field.setAccessible(true);
        assertNull(field.get(pet));
        assertNotNull(pet.getTags());
        assertNotNull(field.get(pet));
    }

    @Test
    public void testGetTags_2() throws Exception {
        final Pet pet = new Pet(17L, "Some Pet");
        final List<Tag> initial = pet.getTags();
        assertNotNull(initial);
        pet.setTags(null);
        final List<Tag> actual = pet.getTags();
        assertNotNull(actual);
        assertFalse(initial == actual);
    }
}