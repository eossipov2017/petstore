package ca.intelliware.petstore.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class BaseEntityTest {

    @Test
    public void getId() throws Exception {
        final long expected = 17L;
        final BaseEntity instance = instance(expected);
        assertEquals((Long) expected, instance.getId());
    }

    private BaseEntity instance(long id) {
        return new BaseEntity(id) {
            private static final long serialVersionUID = 1781484206454795884L;

            @Override
            public BaseEntity copy() {
                throw new UnsupportedOperationException("The method is not implemented yet.");
            }
        };
    }

    @Test
    public void setId() throws Exception {
        final long expected = 31L;
        final BaseEntity instance = instance(0L);
        instance.setId(expected);
        assertEquals((Long) expected, instance.getId());
    }

    @Test
    public void testToString() throws Exception {
        final String expected = "{id=47}";
        final BaseEntity instance = instance(47L);
        assertEquals(expected, instance.toString());
    }
}