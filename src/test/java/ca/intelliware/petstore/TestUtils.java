package ca.intelliware.petstore;

import ca.intelliware.petstore.model.BaseEntity;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static java.util.Spliterator.*;
import static java.util.Spliterators.*;
import static java.util.stream.StreamSupport.*;
import static org.junit.Assert.*;

public class TestUtils {

    private static <T> void compareFields(Field field, T obj1, T obj2) {
        final Class<?> type = field.getType();
        try {
            if (BaseEntity.class.isAssignableFrom(type)) {
                compareValues(field.get(obj1), field.get(obj2));
            } else if (Collection.class.isAssignableFrom(type)) {
                final ArrayList<?> list1 = new ArrayList<>((Collection<?>) field.get(obj1));
                final ArrayList<?> list2 = new ArrayList<>((Collection<?>) field.get(obj2));
                assertEquals(field.getName() + " [size]", list1.size(), list2.size());
                for (int i = 0; i < list1.size(); i++) {
                    final Object item1 = list1.get(i);
                    if (item1 instanceof BaseEntity) {
                        compareValues(item1, list2.get(i));
                    } else {
                        assertEquals(field.getName() + " [" + i + "]", item1, list2.get(i));
                    }
                }
            } else {
                assertEquals(field.getName(), field.get(obj1), field.get(obj2));
            }
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <T> void compareValues(T obj1, T obj2, String... exclusion) {
        if (obj1 == obj2) return;
        if (obj1 == null || obj2 == null) {
            fail("Expected:\t" + obj1 + "\nActual:\t" + obj2);
        }
        if (!obj1.getClass().isAssignableFrom(obj2.getClass())) {
            fail("Expected:\t" + obj1.getClass() + "\nActual:\t" + obj2.getClass());
        }
        final Stream<Field> fields = stream(spliteratorUnknownSize(new ParentIterator(obj1.getClass()), IMMUTABLE),
            false)
            .flatMap(c -> Stream.of(c.getDeclaredFields()))
            .peek(field -> field.setAccessible(true))
            .filter(field -> (field.getModifiers() & Modifier.STATIC) == 0)
            .filter(field -> notContains(exclusion, field.getName()));

        fields.forEach(field -> compareFields(field, obj1, obj2));
    }

    private static boolean notContains(String[] values, String value) {
        return Stream.of(values).noneMatch(v -> v.equals(value));
    }

    static class ParentIterator
        implements Iterator<Class<?>> {

        private Class<?> clazz;

        ParentIterator(Class<?> clazz) {
            this.clazz = clazz;
        }

        /**
         * Returns {@code true} if the iteration has more elements. (In other words, returns {@code true} if {@link
         * #next} would return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return clazz != null && !Object.class.equals(clazz);
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public Class<?> next() {
            if (!hasNext()) throw new NoSuchElementException();
            final Class<?> element = this.clazz;
            this.clazz = this.clazz.getSuperclass();
            return element;
        }
    }
}
